﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldLady : MonoBehaviour {

    Rigidbody2D lady;

    new Collider2D collider;

    public GameObject player;
    public GameObject chaser;

    public bool onPlayerLevel;
    public bool isFallingNow;
    public bool isGrounded;
    public bool canThink;

    public float distanceFromPlayer;
    public float jumpHeight;
    public float runSpeed;

    public int level;


	void Start () {

        lady = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();

     }
	
	
	void Update ()
    {

        lady.velocity = new Vector2(runSpeed, lady.velocity.y);

        if (isFallingNow == true)
        {

            lady.velocity = new Vector2(runSpeed - 4f, lady.velocity.y);

        }


        //RaycastHit2D hit;

        //hit = Physics2D.Raycast(transform.position, transform.up * -1);

        //float groundDistance = 0;

        //if(hit.collider.gameObject.tag == "Platform" || hit.collider.gameObject.tag == "Ground")
        //{
        //    groundDistance = Vector3.Distance(transform.position, hit.point);
        //    Debug.Log(groundDistance);
        //}

        if (lady.velocity.y < 0)
        {

            isFallingNow = true;

        } else
        {
            isFallingNow = false;
        }

        if (canThink == true)
        {
            Debug.Log("Thinking");
            Think();
        }
       
           
    }

    private void FixedUpdate()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D trigger)
    {
        if (trigger.gameObject.tag == "PlatformDecisionTrigger")
        {
            canThink = true;
        }
        if (trigger.gameObject.tag == "Wall")
        {
             runSpeed = runSpeed - 6f;
        }

    }
    

    public void Jump()
    {
        lady.AddForce(new Vector2(0, jumpHeight));
        
    }
    public void Think()
    {

        {
            if (player.transform.position.y > chaser.transform.position.y + 1)
            {
                Jump();
                canThink = false;
            }
        }
    }

    public void Teleport()
    {
        if (player.transform.position.x - chaser.transform.position.x > 10f)
        {
            chaser.transform.position = new Vector2(player.transform.position.x - 5, player.transform.position.y);
        }
    }
    private void OnTriggerStay2D(Collider2D trig)
    {
        if (trig.gameObject.tag == "Platform")
        {
            if (!isFallingNow)
            {
                Physics2D.IgnoreCollision(GetComponent<CapsuleCollider2D>(), trig.gameObject.GetComponent<BoxCollider2D>(), true);

            }
            else
            {
                Physics2D.IgnoreCollision(GetComponent<CapsuleCollider2D>(), trig.gameObject.GetComponent<BoxCollider2D>(), false);
            }
        }
        if (trig.gameObject.tag == "Wall")
        {
            
                Physics2D.IgnoreCollision(GetComponent<CapsuleCollider2D>(), trig.gameObject.GetComponent<BoxCollider2D>(), true);
           
        }
        

    }

   
    private void OnTriggerExit2D(Collider2D trigger)
    {
        if (trigger.gameObject.tag == "Wall")
        {

                     runSpeed = runSpeed + 6f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Platform" || collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform" || collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }

}
