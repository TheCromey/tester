﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

    public string SceneName;
	
	void Start () {

      
	}
	
	
	void Update () {
		
	}
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        
    }

    public void LoadLevel1Tut()
    {
        SceneManager.LoadScene("Level1Tut");
        Time.timeScale = 1f;
    }
    public void LoadLevel2Tut()
    {
        SceneManager.LoadScene("Level2Tut");
        Time.timeScale = 1f;
    }
    public void Quit()
    {
        Debug.Log("Player has Quit.");
        Application.Quit();

    }
    public void LoadLevel_01()
    {
        SceneManager.LoadScene("Level_01");
        Time.timeScale = 1f;
    }
    public void LoadLevel3Tut()
    {
        SceneManager.LoadScene("Level3Tut");
        Time.timeScale = 1f;
    }
    public void LoadLevel4Tut()
    {
        SceneManager.LoadScene("Level4Tut");
        Time.timeScale = 1f;
    }
    public void LoadLevel5Tut()
    {
        SceneManager.LoadScene("Level5Tut");
        Time.timeScale = 1f;
    }
    public void LoadWhiteBox()
    {
        SceneManager.LoadScene("WhiteBox");
        Time.timeScale = 1f;
    }

}
