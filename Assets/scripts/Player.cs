﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    Rigidbody2D body;

    public GameObject winUI;
    public GameObject lossUI;
   
    public float speed;
    public float jumpHeight;
    public float speedTimeLeft = 0f;
    public float jumpTimeLeft = 0f;
    public float speedTime;

    public bool inAir;
    public bool isFaast;
    public bool hasHops;
    public bool isFalling;
       
    CapsuleCollider2D capsuleCollider;

    //Just initiating everything in here.
	void Start () {
        body = GetComponent<Rigidbody2D>();
        capsuleCollider = GetComponent<CapsuleCollider2D>();

	}


    void Update() {

        //This is just going to manage the Jump Perk Timer.
            if (hasHops == true)
            {
                jumpTimeLeft -= Time.deltaTime;
                if (jumpTimeLeft < 0)
                {
                    hasHops = false;
                }
            }
        //This is making our player jump.
        if (Input.GetButtonDown("Fire1"))
        {
            if (inAir == false)
            {
                Debug.Log("Player Jumped");
                body.AddForce(new Vector2(0, jumpHeight));
                inAir = true;

            if (hasHops == true)
        {
            body.AddForce(new Vector2(0, jumpHeight * 1.1f));

         inAir = true;
            }
            }
           
        }
        //This is checking if our player is falling and making them fall faster.
        if (body.velocity.y < 0)
        { 
            body.velocity = new Vector2(speed, body.velocity.y * 1.04f);

            isFalling = true;
        }
        else
        { 
            //If not then it doesnt.
            body.velocity = new Vector2(speed, body.velocity.y);

            isFalling = false;
        }
           
        //This increases the speed of the player when the perk is enabled.
        if (isFaast == true)
        {
           
            body.velocity = new Vector2(speed * 4f, body.velocity.y);
            speedTimeLeft -= Time.deltaTime;

            if (speedTimeLeft < 0)
            {
                isFaast = false;
            }

        }
        

    }
    
    void OnCollisionEnter2D(Collision2D col)
    {
        //This is saying if the player is on the ground then they can jump.
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "Platform")
        {
            inAir = false;
            Debug.Log("Grounded");
        }
        //If the player is touched by the chaser, then they lose.
        if (col.gameObject.tag == "Chaser")
        {
            Debug.Log("Player was caught...");
            lossUI.SetActive(true);
            Time.timeScale = 0f;
        }
        //If the player runs into a wall, then they lose.
        if (col.gameObject.tag == "Wall")
        {
            Debug.Log("Player was too slow...");
            lossUI.SetActive(true);
            Time.timeScale = 0f;
        }
        

    }
    
    private void OnTriggerEnter2D(Collider2D trigger)
    {
        //When the player hits the perk trigger, then the perk/function is called and activates.
        if (trigger.gameObject.tag == "SpeedBoost")
        {
            speedTimeLeft = speedTime;
            isFaast = true;
            Debug.Log("Grabbed Speed Upgrade!");
        }

        if (trigger.gameObject.tag == "JumpUpgrade")
        {
            jumpTimeLeft = 5f;
            hasHops = true;
            Debug.Log("Grabbed Jump Upgrade!");
        }
        //If the player makes it to the caravan then they win the game.
        if (trigger.gameObject.tag == "WinCondition")
        {
            Debug.Log("Player just beat the Level!!");
            winUI.SetActive(true);
            Time.timeScale = 0f;
        } 
    }


    private void OnTriggerStay2D(Collider2D trigger)
    {
        if (trigger.gameObject.tag == "Platform")
        {
            //allows the player to ignore the colliders in the platforms and go through them when they are jumping.
            if (!isFalling)
            {
                Physics2D.IgnoreCollision(GetComponent<CapsuleCollider2D>(), trigger.gameObject.GetComponent<BoxCollider2D>(),true);
                
            } else
            {
                Physics2D.IgnoreCollision(GetComponent<CapsuleCollider2D>(), trigger.gameObject.GetComponent<BoxCollider2D>(),false);
            }

        }
       
    }
    


}
