﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentScript : MonoBehaviour {

    public float speed;

    public int layer;

    Rigidbody2D body;
	
	void Start () {

        body = GetComponent<Rigidbody2D>();

	}
	
	
	void Update () {

        body.velocity = new Vector2(speed, 0);

	}
}
